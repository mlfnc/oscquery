#!/usr/bin/env python

import logging
from textwrap import indent
from argparse import ArgumentParser, RawTextHelpFormatter
from liblo import Address, AddressError, Message, Server, ServerError
import time
import sys
import numpy

__author__ = 'mlfnc'
logger = logging.getLogger('oscquery')

types_info = indent("*Following types are available:\n"
                    "i - 32bit integer\n"
                    "h - 64bit integer\n"
                    "f - 32bit floating point number\n"
                    "d - 64bit (double) floating point number\n"
                    "s - string\n"
                    "S - symbol\n"
                    "c - char\n"
                    "m - 4 byte midi packet (8 digits hexadecimal)\n"
                    "T - TRUE (no value required)\n"
                    "F - FALSE (no value required)\n"
                    "N - NIL (no value required)\n"
                    "I - INFINITUM (no value required)",
                    "    ",
                    lambda line: int(line.count("*")) == 0)

parser = ArgumentParser(description="query osc servers and print the result to stdout",
                        epilog=types_info,
                        usage="%(prog)s [options] port path [types] [values [values ...]]",
                        formatter_class=lambda prog: RawTextHelpFormatter(prog,
                                                                          indent_increment=4,
                                                                          max_help_position=100))
parser.add_argument("port", help="target port")
parser.add_argument("path", help="target path")

additional_settings = parser.add_argument_group("program settings")
additional_settings.add_argument("types", help="types of the defined values*", nargs="?", default="")
additional_settings.add_argument("values", help="value(s)", nargs="*", default="")
additional_settings.add_argument("-t", "--timeout", help="timeout in ms (default: 1000)",
                                 default="1", required=False)
additional_settings.add_argument("-q", "--quiet", help="quiet mode (loglevel: error)", required=False,
                                 action="store_true")
additional_settings.add_argument("-v", "--verbose", help="verbose mode (loglevel: debug)", required=False,
                                 action="store_true")
additional_settings.add_argument("-l", "--long", help="long log mode (adds timestamp and loglevel)", required=False,
                                 action="store_true")

osc_settings = parser.add_argument_group("osc settings")
osc_settings.add_argument("-H", "--host", default="localhost", help="target host (default: localhost", required=False)
osc_settings.add_argument("-p", "--protocol", default="udp", help="protocol used (default: udp)", required=False)
args, unknownargs = parser.parse_known_args()

if args.long:
    FORMAT = '%(asctime)-15s %(levelname)s %(message)s'
else:
    FORMAT = '%(message)s'

logging.basicConfig(format=FORMAT)

if args.quiet:
    logger.setLevel(logging.ERROR)
if args.verbose:
    logger.setLevel(logging.DEBUG)

if args.timeout.isdigit():
    timeout = int(args.timeout)
else:
    timeout = 1000
    logger.warning("Invalid format: timeout %s" % str(args.timeout))

t_end = time.time() + timeout
server = Server()

try:
    target_message = Message(path=args.path)
except AddressError as err:
    logger.error("Invalid format: path %s" % str(args.path))
    sys.exit(2)

try:
    target_address = Address(args.host, args.port)
except AddressError as err:
    logger.error("Invalid format: host or port")
    sys.exit(2)


def settype(val_type, val):
    retval = None
    returncode = 0
    if val_type == "s":
        retval = str(val)
    elif val_type == "i":
        retval = numpy.int32(val)
    elif val_type == "h":
        retval = numpy.int64(val)
    elif val_type == "f":
        retval = numpy.float32(val)
    elif val_type == "d":
        numpy.float64(val)
    else:
        logger.warning("Type \" %s \" not found, ignored" % str(val_type))

    return returncode, retval


if args.types is not None:
    value_type = None

    for idx, arg_type in enumerate(args.types):
        arg_type_index = idx
        arg_value = args.values[arg_type_index]
        rcode, arg_value_type = settype(arg_type, arg_value)
        if rcode == 0:
            target_message.add((arg_type, arg_value_type))
            logger.debug("Value added: %s %s" % (str(arg_type), str(arg_value)))
        else:
            logger.warning("Value invalid: %s" % str(arg_type))


def callback(cb_path, cb_args, cb_types, cb_src):
    logger.debug("Response received for path %s from %s" % (str(cb_path), str(cb_src.url)))
    for idxargs, arg in enumerate(cb_args):
        restype = str(cb_types[idxargs])
        resarg = str(arg)
        logger.debug("Response recieved for path %s: %s %s" % (str(cb_path), str(cb_types[idxargs]), str(arg)))
        logger.warning("Response %s: %s" % (str(restype), str(resarg)))
    sys.exit(0)


server.add_method(None, None, callback)
server.send(target_address, target_message)


try:
    server.recv(timeout)
except ServerError as err:
    logger.warning("No Answer received. You are on your own...")
