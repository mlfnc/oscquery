# oscquery

query osc servers and print the result to stdout

## usage

usage: oscquery.py [options] port path [types] [values [values ...]]

query osc servers and print the result to stdout

positional arguments:  
    port                              target port  
    path                              target path  

optional arguments:  
    -h, --help                        show this help message and exit  

program settings:  
    types                             types of the defined values*  
    values                            value(s)  
    -t TIMEOUT, --timeout TIMEOUT     timeout in ms (default: 1000)  
    -q, --quiet                       quiet mode (loglevel: error)  
    -v, --verbose                     verbose mode (loglevel: debug)  
    -l, --long                        long log mode (adds timestamp and loglevel)  

osc settings:  
    -H HOST, --host HOST              target host (default: localhost  
    -p PROTOCOL, --protocol PROTOCOL  protocol used (default: udp)  

*Following types are available:  
    i - 32bit integer  
    h - 64bit integer  
    f - 32bit floating point number  
    d - 64bit (double) floating point number  
    s - string  
    S - symbol  
    c - char  
    m - 4 byte midi packet (8 digits hexadecimal)  
    T - TRUE (no value required)  
    F - FALSE (no value required)  
    N - NIL (no value required)  
    I - INFINITUM (no value required)  

## license

This project is licensed under the GPLv3 License - see the [LICENSE.md](LICENSE.md) file for details
